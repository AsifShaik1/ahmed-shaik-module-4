import 'package:flutter/material.dart';
import 'dashboard.dart';

class Feature2 extends StatefulWidget {
  const Feature2({Key? key}) : super(key: key);

  @override
  State<Feature2> createState() => _Feature2();
}

class _Feature2 extends State<Feature2> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Feature 2")),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('feature2.jpg', width: 200, fit: BoxFit.fitWidth),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Dashboard()),
                  );
                },
                child: const Text('Goto Dashboard'),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Dashboard()),
          );
        },
        child: const Icon(Icons.home),
      ),
    );
  }
}
