import 'package:flutter/material.dart';
import 'package:module_4_code/login.dart';
//import 'dashboard.dart';

class ForgotLogin extends StatefulWidget {
  const ForgotLogin({Key? key}) : super(key: key);

  @override
  State<ForgotLogin> createState() => _ForgotLogin();
}

class _ForgotLogin extends State<ForgotLogin> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Forgot Login")),
      body: Center(
        // heightFactor: 3,
        // widthFactor: 0.8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Text('Your login information will be emailed to you.'),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  // Validate will return true if the form is valid, or false if
                  // the form is invalid.
                  //if (_formKey.currentState!.validate()) {
                  // Process data.
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const LoginPage()),
                  );
                  //}
                },
                child: const Text('Goto Login'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
