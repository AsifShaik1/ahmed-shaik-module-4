/*
- Add an animation splash screen (done).
- Create a common theme for your app, from previous class.
- Implement design onto your app
*/

import 'package:flutter/material.dart';
import 'login.dart';
//import 'profile.dart';
//import 'dashboard.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'MTN App Module 4 Code';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: AppTheme().themedata,
      /*theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.orange,

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),*/

      /*
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const LoginPage(),
        //body: const Profile()
        //body: const Dashboard(),
      ),
      */
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      duration: 1500,
      splash: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Module 4 Code',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            Image.asset('assets/balloons.png',
                width: 200,
                //height: 150,
                fit: BoxFit.fill)
          ],
        ),
      ),
      nextScreen: const LoginPage(),
      splashTransition: SplashTransition.slideTransition,
      pageTransitionType: PageTransitionType.fade,
      //alignment: Alignment.topCenter,
      backgroundColor: Colors.blueAccent,
      splashIconSize: 400,
    );
  }
}
