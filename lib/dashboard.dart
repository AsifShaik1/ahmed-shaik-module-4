import 'package:flutter/material.dart';
import 'login.dart';
import 'feature1.dart';
import 'feature2.dart';
import 'profile.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Dashboard")),
      body: Center(
        // heightFactor: 3,
        // widthFactor: 0.8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                    onPressed: () {
                      //Navigate
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginPage()));
                    },
                    child: const Text('Login Page'))),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                    onPressed: () {
                      //Navigate
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Feature1()),
                      );
                    },
                    child: const Text('Feature Page 1'))),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                    onPressed: () {
                      //Navigate
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Feature2()),
                      );
                    },
                    child: const Text('Feature Page 2'))),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                    onPressed: () {
                      //Navigate
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Profile()),
                      );
                    },
                    child: const Text('Profile')))
          ],
        ),
      ),
    );
  }
}
