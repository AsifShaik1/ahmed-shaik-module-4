import 'package:flutter/material.dart';

class AppTheme {
  ThemeData themedata = ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.white,

      // Define the default font family.
      fontFamily: 'Georgia',
      appBarTheme: const AppBarTheme(
        color: Colors.black45,
      ),
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          primary: Colors.blueGrey,
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: Colors.black45,
        ),
      ),
      //floatingActionButtonTheme: FloatingActionButtonThemeData(
      //  style: FloatingActionButton.styleFrom(
      //    primary: Colors.black45,
      //  ),
      //),
      scaffoldBackgroundColor: Colors.blueGrey,
      textTheme: const TextTheme(
              bodyText1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
              bodyText2: TextStyle(
                  fontSize: 14.0,
                  fontFamily: 'Hind',
                  fontStyle: FontStyle.italic),
              headline1: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
              headline2: TextStyle(fontSize: 44.0, fontWeight: FontWeight.bold),
              headline3: TextStyle(fontSize: 38.0, fontWeight: FontWeight.bold),
              headline4: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
              headline5: TextStyle(fontSize: 26.0, fontWeight: FontWeight.bold),
              headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic))
          .apply(
        bodyColor: Colors.white,
      ));
}
